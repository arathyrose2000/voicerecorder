


import { Buffer } from 'buffer';
import { StyleSheet, Text, View, Button } from 'react-native';
import AudioRecord from 'react-native-audio-record';
import React, { Component } from 'react';
import Sound from 'react-native-sound';
import { PermissionsAndroid } from 'react-native';



const URL = "ws://api.mashinga.com:6000"

export default class App extends Component {
  constructor(props) {
    super(props);
    this.sound = null;
    this.state = {
      audioFile: '',
      recording: false,
      loaded: false,
      paused: true,
      open: false,
      text: "",
      data: ""
    };
    this.socket = new WebSocket(URL);
    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.load = this.load.bind(this);

  }


  async componentDidMount() {
    await this.checkPermission();
    this.socket.onopen = () => {
      // on connecting, do nothing but log it to the console
      console.log('connected')
    }
    this.socket.onmessage = ({ data }) => {
      console.log(data, JSON.parse(data).payload)
      if (data) { tt = this.state.text + JSON.parse(data).payload; this.setState({ text: tt }) }
    }
    const options = {
      sampleRate: 8000,
      channels: 1,
      bitsPerSample: 16,
      wavFile: 'test.wav'
    };

    AudioRecord.init(options);

    AudioRecord.on('data', data => {
      const chunk = Buffer.from(data, 'base64');
      console.log('Data: ', data)
      console.log('Type of Data: ', typeof data)
      console.log('Chunk size', chunk.byteLength);
      console.log('Chunk: ', chunk)
      console.log('Chunk type: ', typeof chunk)
      //  this.socket.send(chunk)
      this.socket.send(chunk)
      this.socket.onmessage = ({ data }) => console.log(JSON.parse(data).payload)
      // do something with audio chunk
      this.setState({ data: data })
    });
  }

  checkPermission = async () => {
    const p = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO);
    console.log('permission check', p);
    if (p === 'authorized') return;
    return this.requestPermission();
  };

  requestPermission = async () => {
    const p = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO);
    console.log('permission request', p);
  };

  start = () => {
    console.log('start record');
    this.setState({ audioFile: '', recording: true, loaded: false });
    AudioRecord.start();
    //this.socket.open()
  };

  stop = async () => {
    if (!this.state.recording) return;
    console.log('stop record');
    let audioFile = await AudioRecord.stop();
    console.log('audioFile', audioFile);
    // send as file


    this.socket.send(Buffer.from(audioFile))


    this.socket.send(JSON.stringify({
      //  type: "contentchange",
      //  username: this.state.username,
      content: audioFile
    }));


    console.log("sent", this.state.paused)
    this.socket.close()
    this.setState({ audioFile, recording: false, paused: true });
  };

  load = () => {
    return new Promise((resolve, reject) => {
      if (!this.state.audioFile) {
        return reject('file path is empty');
      }

      this.sound = new Sound(this.state.audioFile, '', error => {
        if (error) {
          console.log('failed to load the file', error);
          return reject(error);
        }
        this.setState({ loaded: true });
        return resolve();
      });
    });
  };

  play = async () => {
    if (!this.state.loaded) {
      try {
        await this.load();
      } catch (error) {
        console.log(error);
      }
    }

    this.setState({ paused: false });
    Sound.setCategory('Playback');

    this.sound.play(success => {
      if (success) {
        console.log('successfully finished playing');
      } else {
        console.log('playback failed due to audio decoding errors');
      }
      this.setState({ paused: true });
      // this.sound.release();
    });
  };

  pause = () => {
    if (this.sound) {
      this.sound.pause();
      console.log("PAUSING")
    }
    this.play()
    this.setState({ paused: true });
  };

  render() {
    const { recording, paused, audioFile } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.row}>
          <Button onPress={this.start} title="Record" disabled={recording} />
          <Button onPress={this.stop} title="Stop" disabled={!recording} />

          {paused == true ? (
            <Button onPress={this.play} title="Play" disabled={!audioFile} />
          ) :
            paused == false ? (
              <Button onPress={this.pause} title="Pause" disabled={!audioFile} />
            ) : <Text> </Text>}
        </View>
        <View style={styles.row}>
          <Text>{this.state.text}</Text>
        </View>
        <View style={styles.row}>
          <Text>{this.state.data}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  }
});