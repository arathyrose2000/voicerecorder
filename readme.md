# Commands to install stuff

npm install --global expo-cli
expo init RECORDER
cd RECORDER

Add NSMicrophoneUsageDescription key to your Info.plist:

<key>NSMicrophoneUsageDescription</key>
<string>Allow $(PRODUCT_NAME) to access your microphone</string>

Add android.permission.RECORD_AUDIO permission to your manifest (android/app/src/main/AndroidManifest.xml):

<uses-permission android:name="android.permission.RECORD_AUDIO" />


